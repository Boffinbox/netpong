﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class RacketController : NetworkBehaviour
{
    float speed = 15f;
    Vector3 playerPos;
    public override void OnStartLocalPlayer()
    {
        base.OnStartLocalPlayer();
        if (GameObject.FindGameObjectsWithTag("Player").Length > 1)
        {
            transform.position = new Vector2(6.3f, 0);
        }
        else
        {
            transform.position = new Vector2(-6.3f, 0);
        }

    }
    private void UpdatePosition(Vector2 transform)
    {
        this.transform.position = transform;
    }
    private void Update()
    {
        if (!isServerOnly)
        {
            if (Input.GetAxisRaw("Vertical") > 0 || Input.GetAxisRaw("Vertical") < 0)
            {
                CmdMove();
            }
            else
            {
                CmdStop();
            }
        }
    }
    [Command]
    private void CmdMove()
    {
        RpcMove();
    }
    private void CmdStop()
    {
        RpcStop();
    }
    [ClientRpc]
    private void RpcMove()
    {
        if (transform.position.y < 2.3f || transform.position.y > -2.3f)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(0, speed * (Input.GetAxisRaw("Vertical")));
        }
        else
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        }
    }
    private void RpcStop()
    {
        GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
    }
}
