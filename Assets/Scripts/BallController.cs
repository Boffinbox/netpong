﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Mirror;

public class BallController : NetworkBehaviour
{
    UnityEvent resetBall;
    Rigidbody2D rb;
    float speed;
    float defaultSpeed = 6f;
    [SyncVar]
    public int blueScore = 0;
    [SyncVar]
    public int redScore = 0;
    [SerializeField] Text blueScoreText;
    [SerializeField] Text redScoreText;
    public override void OnStartServer()
    {
        base.OnStartServer();
        speed = defaultSpeed;
        rb = GetComponent<Rigidbody2D>();
        if (resetBall == null)
        {
            resetBall = new UnityEvent();
        }
        resetBall.AddListener(ResetBall);
        rb.velocity = new Vector2(-speed, 0);
    }
    [ServerCallback]
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            GameObject racketObject = collision.gameObject;
            Vector2 racketPos = racketObject.transform.position;
            Vector2 currentPos = this.transform.position;
            Vector2 newDirection = currentPos - racketPos;
            speed = speed * 1.05f;
            if (speed > (10 * defaultSpeed))
            {
                speed = (10 * defaultSpeed);
            }
            rb.velocity = newDirection.normalized * speed;
        }
        if (collision.gameObject.tag == "Score")
        {
            ResetBall();
        }
    }
    private void ResetBall()
    {
        speed = defaultSpeed;
        if (transform.position.x < 0)
        {
            RpcUpdateScoreBlue();
        }
        if (transform.position.x > 0)
        {
            RpcUpdateScoreRed();
        }
        rb.MovePosition(new Vector2(0, 0));
    }
    [ClientRpc]
    private void RpcUpdateScoreBlue()
    {
        blueScore++;
        blueScoreText.text = blueScore.ToString();
    }
    [ClientRpc]
    private void RpcUpdateScoreRed()
    {
        redScore++;
        redScoreText.text = redScore.ToString();
    }
}
